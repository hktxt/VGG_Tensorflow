# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 21:37:34 2017

@author: Max
"""
#%%
import numpy as np
import tensorflow as tf
import math
import input_data
import VGG
import tools

#%%

IMG_W = 64
IMG_H = 64
N_CLASSES = 2
BATCH_SIZE = 20
IS_PRETRAIN = True
dropout = 1#for testing set dropout = 1
CAPACITY = 1000

def evaluate():
    with tf.Graph().as_default():
        test_dir = 'D:/DATA/cats_vs_dogs/64/test/'
        log_dir = 'C:/Users/Max/Downloads/My-TensorFlow-tutorials-master/04 VGG Tensorflow/cat_vs_dog/logs2/train/'
        #n_test = 8000
                
        test_images, test_labels = input_data.get_files(test_dir, 0)
        n_test = len(test_labels)
        test_image_batch, test_label_batch = input_data.get_batch(test_images, test_labels, IMG_W, IMG_H, BATCH_SIZE, CAPACITY)
        
        logits = VGG.VGG16N(test_image_batch, N_CLASSES, dropout, IS_PRETRAIN)
        correct = tools.num_correct_prediction(logits, test_label_batch)
        saver = tf.train.Saver(tf.global_variables())
        
        with tf.Session() as sess:
            
            print("Reading checkpoints...")
            ckpt = tf.train.get_checkpoint_state(log_dir)
            if ckpt and ckpt.model_checkpoint_path:
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                saver.restore(sess, ckpt.model_checkpoint_path)
                print('Loading success, global_step is %s' % global_step)
            else:
                print('No checkpoint file found')
                return
        
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess = sess, coord = coord)
            
            try:
                print('\nEvaluating......')
                num_step = int(math.floor(n_test / BATCH_SIZE))
                num_sample = num_step*BATCH_SIZE
                step = 0
                total_correct = 0
                while step < num_step and not coord.should_stop():
                    batch_correct = sess.run(correct)
                    #print(batch_correct)
                    total_correct += np.sum(batch_correct)
                    #total_correct += batch_correct
                    step += 1
                print('Total testing samples: %d' %num_sample)
                print('Total correct predictions: %d' %total_correct)
                print('Average accuracy: %.2f%%' %(100*total_correct/num_sample))
            except Exception as e:
                coord.request_stop(e)
            finally:
                coord.request_stop()
                coord.join(threads)
#%%
evaluate()