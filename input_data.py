
import tensorflow as tf
import numpy as np
import os
import math

#%%

#change this to your data directory
#file path and ratio
def get_files(path, ratio):
    
    cats = []
    label_cats = []
    dogs = []
    label_dogs = []
    for file in os.listdir(path):
        name = file.split(sep='.')
        if name[0]=='cat':
            cats.append(path + file)
            label_cats.append(0)
        else:
            dogs.append(path + file)
            label_dogs.append(1)
    #print('There are %d cats\nThere are %d dogs' %(len(cats), len(dogs)))
    cats = cats[0:1000]
    dogs = dogs[0:100]
    label_cats = label_cats[0:1000]
    label_dogs = label_dogs[0:100]
    print('There are %d cats\nThere are %d dogs' %(len(cats), len(dogs)))
    image_list = np.hstack((cats, dogs))
    label_list = np.hstack((label_cats, label_dogs))
    
    temp = np.array([image_list, label_list])
    temp = temp.transpose()
    np.random.shuffle(temp)   
    
    all_image_list = temp[:, 0]
    all_label_list = temp[:, 1]
    
    if ratio==0:
        all_label_list = [int(float(i)) for i in all_label_list]
        return all_image_list, all_label_list
    else:
        n_sample = len(all_label_list)
        n_val = math.ceil(n_sample*ratio) # number of validation samples
        n_train = n_sample - n_val # number of trainning samples
        
        tra_images = all_image_list[0:n_train]
        tra_labels = all_label_list[0:n_train]
        tra_labels = [int(float(i)) for i in tra_labels]
        val_images = all_image_list[n_train:-1]
        val_labels = all_label_list[n_train:-1]
        val_labels = [int(float(i)) for i in val_labels]
        
        
        
        return tra_images,tra_labels,val_images,val_labels


#%%

def get_batch(image, label, image_W, image_H, batch_size, capacity):
    
    image = tf.cast(image, tf.string)
    label = tf.cast(label, tf.int32)

    # make an input queue
    input_queue = tf.train.slice_input_producer([image, label])
    
    label = input_queue[1]
    image_contents = tf.read_file(input_queue[0])
    image = tf.image.decode_jpeg(image_contents, channels=3)
    
    ######################################
    # data argumentation should go to here
    ######################################
    
    image = tf.image.resize_image_with_crop_or_pad(image, image_W, image_H)    
    # if you want to test the generated batches of images, you might want to comment the following line.
    
    image = tf.image.per_image_standardization(image)
    
    image_batch, label_batch = tf.train.batch([image, label],
                                                batch_size= batch_size,
                                                num_threads= 64, 
                                                capacity = capacity)
    n_classes = 2
    label_batch = tf.one_hot(label_batch, depth= n_classes)
    #label_batch = tf.cast(label_batch, dtype=tf.int32)
    label_batch = tf.reshape(label_batch, [batch_size, n_classes])
    #label_batch = tf.reshape(label_batch, [batch_size])
    image_batch = tf.cast(image_batch, tf.float32)
    
    return image_batch, label_batch


 






    
