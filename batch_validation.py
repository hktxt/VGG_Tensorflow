# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 21:43:56 2017

@author: Max
"""

#%% TEST
# To test the generated batches of images
# When training the model, DO comment the following codes
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import input_data

#%%
IMG_W = 64
IMG_H = 64
N_CLASSES = 2
BATCH_SIZE = 5
IS_PRETRAIN = True
CAPACITY = 1000

train_dir = 'D:/DATA/cats_vs_dogs/64/train/'

tra_images, tra_labels = input_data.get_files(train_dir, 0)
tra_image_batch, tra_label_batch = input_data.get_batch(tra_images, tra_labels, IMG_W, IMG_H, BATCH_SIZE, CAPACITY)



with tf.Session() as sess:
    i = 0
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    
    try:
        while not coord.should_stop() and i<1:
            
            img, label = sess.run([tra_image_batch, tra_label_batch])
            
            # just test one batch
            for j in np.arange(BATCH_SIZE):
                #print('label: %d' %label[j])
                print(label[j])
                plt.imshow(img[j,:,:,:])
                plt.show()
            i+=1
            
    except tf.errors.OutOfRangeError:
        print('done!')
    finally:
        coord.request_stop()
    coord.join(threads)


#%%