# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 21:55:35 2017

@author: Max
"""
#%% Evaluate one image

import numpy as np
import tensorflow as tf
import input_data
from PIL import Image
import matplotlib.pyplot as plt
import VGG

IS_PRETRAIN = True
test_dir = 'D:/DATA/cats_vs_dogs/64/test/'

def get_one_image(train,is_random):
    '''Randomly pick one image from training data
    Return: ndarray
    '''
    if is_random == 1:
        n = len(train)
        ind = np.random.randint(0, n)
        img_dir = train[ind]
    
        image = Image.open(img_dir)
        plt.imshow(image)
        #image = image.resize([208, 208])
        image = np.array(image)
        return image
    else:
        #change im_dir and test your picked image
        img_dir = 'D:/DATA/hj/64/test/normal/013996.jpg'
        image = Image.open(img_dir)
        plt.imshow(image)
        image = np.array(image)
        return image

def evaluate_one_image():
    '''Test one image against the saved models and parameters'''
    train_images, train_labels = input_data.get_files(test_dir,0)
    image_array = get_one_image(train_images,1)
    with tf.Graph().as_default():
        N_CLASSES = 2
        
        image = tf.cast(image_array, tf.float32)
        image = tf.image.per_image_standardization(image)
        image = tf.reshape(image, [1, 64, 64, 3])
        
        logit = VGG.VGG16N(image, N_CLASSES, 1, IS_PRETRAIN)
        logit = tf.nn.softmax(logit)
        #print(logit)   
        x = tf.placeholder(tf.float32, shape=[64, 64, 3])
        
        # you need to change the directories to yours.
        logs_train_dir = 'C:/Users/Max/Downloads/My-TensorFlow-tutorials-master/04 VGG Tensorflow/cat_vs_dog/logs2/train/' 
                       
        saver = tf.train.Saver()
        
        with tf.Session() as sess:
            
            print("Reading checkpoints...")
            ckpt = tf.train.get_checkpoint_state(logs_train_dir)
            if ckpt and ckpt.model_checkpoint_path:
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                saver.restore(sess, ckpt.model_checkpoint_path)
                print('Loading success, global_step is %s' % global_step)
            else:
                print('No checkpoint file found')
            
            prediction = sess.run(logit, feed_dict={x: image_array})
            print(prediction)
            max_index = np.argmax(prediction)
            if max_index==0:
                print('This is cat with possibility %.6f' %prediction[:, 0])
            else:
                print('This is dog with possibility %.6f' %prediction[:, 1])
                
#%%
evaluate_one_image()